import { gql } from 'apollo-server-express';

export const typeDefs = gql`
  type Query {
    getUsers: [User]
    getProducts: [Product]
    getProduct(slug: String, _id: String): Product
    getOrder(orderNumber: Float): Order
  }

  type User {
    _id: ID
    first_name: String
    last_name: String
    contact_email: String
    paypal_email: String
    day_birth: String
    month_bith: String
    year_birth: String
  }
  type Product {
    _id: ID
    userId: String
    title: String
    body: String
    image: String
    price: Float
    status: String
    slug: String
  }
  type Order {
    _id: ID
    orderNumber: Float
    clientName: String
    clientEmail: String
    clientMessage: String
    clientLink: String
  }
`;
