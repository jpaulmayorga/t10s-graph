import { usersQueries, productsQueries, ordersQueries } from '../models';

export const resolvers = {
  Query: {
    ...usersQueries,
    ...productsQueries,
    ...ordersQueries,
  },
};
