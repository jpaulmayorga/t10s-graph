import express from 'express';
import { ApolloServer } from 'apollo-server-express';

import { typeDefs, resolvers } from './graphql';

export function init() {
  const apolloServer = new ApolloServer({ typeDefs, resolvers });
  const app = express();

  apolloServer.applyMiddleware({ app });

  return app;
}

if (require.main === module) {
  // called directly i.e. "node app"
  init().listen(3030, (err: Error) => {
    if (err) {
      // tslint:disable-next-line: no-console
      console.error(err);
    }
    // tslint:disable-next-line: no-console
    console.log('GraphQL server listening on http://locahost:3000/');
  });
} else {
  // required as a module => executed on aws lambda
}
