import { connectMongo } from '../../utils/database';
import { ObjectID } from 'mongodb';

export const productsQueries = {
  getProducts: async () => {
    try {
      const db = await connectMongo();
      const data = await db.collection('products').find().toArray();
      return data;
    } catch (error) {
      console.log(error);
      return [];
    }
  },
  getProduct: async (_root: any, args: { slug: string; _id: string }) => {
    try {
      const db = await connectMongo();
      if (args.slug) {
        const data = await db.collection('products').findOne({ slug: args.slug });
        return data;
      }
      const data = await db.collection('products').findOne({ _id: new ObjectID(args._id) });
      return data;
    } catch (error) {
      console.log(error);
      return [];
    }
  },
};
