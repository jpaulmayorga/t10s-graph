import { connectMongo } from '../../utils/database';

export const ordersQueries = {
  getOrder: async (_root: any, args: { orderNumber: Number }) => {
    try {
      const db = await connectMongo();
      const data = await db.collection('orders').findOne({ orderNumber: args.orderNumber });
      return data;
    } catch (error) {
      console.log(error);
      return [];
    }
  },
};
