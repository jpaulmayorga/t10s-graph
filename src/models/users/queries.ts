import { connectMongo } from '../../utils/database';

export const usersQueries = {
  getUsers: async () => {
    try {
      const db = await connectMongo();
      const data = await db.collection('users').find().toArray();
      return data;
    } catch (error) {
      console.log(error);
      return [];
    }
  },
};
